using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] private PlayerBehaviour _playerBehaviour = default;

    [SerializeField] private TextMeshProUGUI _diceRollText = default;
    [SerializeField] private TextMeshProUGUI _damageText = default;
    [SerializeField] private Slider _healthBar = default;
    [SerializeField] private Button _attackButton = default;

    private void Start()
    {
        SubscribeToEvents();
    }

    private void SubscribeToEvents()
    {
        _attackButton.onClick.AddListener(_playerBehaviour.Player.Attack);

        _playerBehaviour.Player.OnPlayerDamaged += UpdateHealthBar;
        _playerBehaviour.Player.OnPlayerAttacked += UpdateDiceText;
    }

    private void UpdateHealthBar()
    {
        _healthBar.value = _playerBehaviour.Player.Health;
    }

    private void UpdateDiceText()
    {
        _diceRollText.text = _playerBehaviour.Player.AttacksDiceRoll.ToString();
        _damageText.text = (_playerBehaviour.Player.Damage * _playerBehaviour.Player.AttacksDiceRoll).ToString();
    }
}
