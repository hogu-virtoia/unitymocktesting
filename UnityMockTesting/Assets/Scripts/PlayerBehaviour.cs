using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    [SerializeField] private PlayerBehaviour _target = default;

    private void Awake()
    {
        Player = new Player();
    }

    private void Start()
    {
        Player.Target = _target.Player;
    }

    public IPlayer Player { get; private set; }
}
