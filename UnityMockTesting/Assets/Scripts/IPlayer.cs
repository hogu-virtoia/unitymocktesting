﻿using System;

public interface IPlayer
{
    event Action OnPlayerDamaged;
    event Action OnPlayerAttacked;
    
    void TakeDamage(int damage);
    void Attack();
    
    int AttacksDiceRoll { get; set; }
    int Health { get; set; }
    int Damage { get; set; }
    
    IPlayer Target { get; set; }
}
