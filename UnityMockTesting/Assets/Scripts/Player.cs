﻿using System;

public class Player : IPlayer
{
    public event Action OnPlayerDamaged;
    public event Action OnPlayerAttacked;

    public void TakeDamage(int damage)
    {
        Health -= damage;
        OnPlayerDamaged?.Invoke();
    }

    public void Attack()
    {
        AttacksDiceRoll = UnityEngine.Random.Range(1, 7);

        for (int i = 0; i < AttacksDiceRoll; i++)
        {
            Target.TakeDamage(Damage);
        }

        OnPlayerAttacked?.Invoke();
    }

    public int AttacksDiceRoll { get; set; }

    public int Health { get; set; } = 100;

    public int Damage { get; set; } = 5;

    public IPlayer Target { get; set; }
}