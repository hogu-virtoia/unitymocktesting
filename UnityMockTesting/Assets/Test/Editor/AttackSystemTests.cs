using Moq;
using NUnit.Framework;

public class AttackSystemTests 
{
    [Test]
    public void PlayerAttacking()
    {
        Mock<IPlayer> playerOne = new Mock<IPlayer>();
        Player playerTwo = new Player();
        
        playerOne.Setup(c => c.Target).Returns(playerTwo);

        playerOne.Object.Attack();
        
        playerOne.Verify(player => player.Attack(), Times.Exactly(1));
    }

    [Test]
    public void PlayerDamagingFromOneToSixTimes()
    {
        Mock<IPlayer> playerOne = new Mock<IPlayer>();
        Player playerTwo = new Player();
        
        playerTwo.Target = playerOne.Object;
        playerTwo.Attack();
        
        playerOne.Verify(player => player.TakeDamage(playerTwo.Damage), Times.Between(1, 6, Range.Inclusive));
    }

    [Test]
    public void PlayerDamagingThreeTimes()
    {
        Mock<IPlayer> playerOne = new Mock<IPlayer>();
        Player playerTwo = new Player();
        
        playerTwo.Target = playerOne.Object;
        playerTwo.Attack();
        
        playerOne.Verify(player => player.TakeDamage(playerTwo.Damage), Times.Exactly(3));
    }
}
